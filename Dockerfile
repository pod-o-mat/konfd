FROM golang:1.10-alpine as build

WORKDIR /go/src/konfd

COPY *.go /go/src/konfd/
RUN GOOS=linux go build -a --ldflags '-extldflags "-static"' -tags netgo -installsuffix netgo .

#################################################

FROM scratch
COPY --from=build /go/src/konfd/konfd /konfd

ENTRYPOINT ["/konfd"]
